# Ticker API

#### GET Endpoint:

https://kojiflowers.com/projects/tickerAPI/

---

#### Query Variables

startDate

* format: mm/dd/yyyy
* required: yes

endDate

* format: mm/dd/yyyy
* required: yes

tickers

* format: array for multiple values or string if single value
* required: yes

groupByWeek

* format: boolean (true/false)
* required: no

---

#### Example queries using API:

###### Example 1

* Start Date: 12/05/2021
* End Date: 01/08/2022
* Tickers: TGT, AMZN, UPS
* Group By Week: yes
* https://kojiflowers.com/projects/tickerAPI/?startDate=12/05/2021&endDate=01/08/2022&tickers\[\]=TGT&tickers\[\]=AMZN&tickers\[\]=UPS&groupByWeek=true

###### Example 2

* Start Date: 12/05/2021
* End Date: 01/08/2022
* Tickers: TGT, AMZN
* Group By Week: no
* https://kojiflowers.com/projects/tickerAPI/?startDate=12/05/2021&endDate=01/08/2022&tickers\[\]=TGT&tickers\[\]=AMZN

###### Example 3

* Start Date: 12/05/2021
* End Date: 01/08/2022
* Tickers: TGT
* Group By Week: no
* https://kojiflowers.com/projects/tickerAPI/?startDate=12/05/2021&endDate=01/08/2022&tickers=TGT
