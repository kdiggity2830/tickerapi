<?php

namespace Api;

use PDO;
use PDOException;

/**
 * Handle DB operations
 */
class DatabaseHandler
{
    protected $submitted;

    /**
     * Executes the process for getting ticker data via DB
     */
    public function getTickerData(array $submitted)
    {
        $this->submitted = $submitted;
        $db = $this->connectToDB();
        $dbResult = $this->executeQuery($db, $submitted);
        if (is_array($dbResult)) {
            $result = $this->groupData($dbResult);
        } else {
            $result = 'Error: Could not process request.';
        }
        
        return $result;
    }

    /**
     * Handles database connection
     */
    protected function connectToDB() {

        include_once("../78394/settings.php");
        $user   = getUser();
        $pass   = getPassword();
        $host   = getHost();
        $dbname = getDbName();

        // open DB connection
        $dbh = new PDO('mysql:host='.$host.';dbname='.$dbname, $user, $pass);
        return $dbh;
    }

    /**
     * Executes the DB query with the submitted values
     */
    protected function executeQuery(PDO $dbh, array $submitted)
    {
        $tickersList = implode("','",$submitted['tickers']);
        $tickersList = "'".$tickersList."'";
        $startDate   = $this->formatDate($submitted['startDate']);
        $endDate     = $this->formatDate($submitted['endDate']);

        // build query
        $queryText = " SELECT comp.name as company_name, comp.ticker, hist.d as date,
        hist.high as high_price, hist.low as low_price, hist.close as closing_price 
        FROM companies as comp
        LEFT JOIN historical as hist
            ON (comp.company_id=hist.company_id) 
        WHERE comp.ticker in ($tickersList)
        and hist.d >= '$startDate'
        and hist.d <= '$endDate' ORDER BY hist.d ASC;";

        try {
            $query = $dbh->prepare($queryText);
            $query->execute();
            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
            $dbh = null; // close DB connection

        } catch (PDOException $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * Group data by week day or week
     */
    protected function groupData(array $dbResult)
    {
        $returnValues = [];

        foreach($dbResult as $key => $valueArray)
        {
            $currentDateValue = $valueArray['date'];
            if ($this->submitted['groupByWeek'] === 'true') {
                $weekNumber = date('W', strtotime($currentDateValue));
                $year       = date('Y', strtotime($currentDateValue));

                $returnValues['Week'.$weekNumber.'-'.$year][] = $valueArray;
            } else {
                $weekday = date('l', strtotime($currentDateValue));

                $returnValues[$weekday][] = $valueArray;
            }
        }
        
        return $returnValues;
    }

    /**
     * Format date for DB query
     */
    protected function formatDate(string $date)
    {
        $date          = date_create($date);
        $formattedDate = date_format($date,"Y-m-d");

        return $formattedDate;
    }


}