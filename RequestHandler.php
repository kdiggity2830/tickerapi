<?php

namespace Api;
use DateTime;

/**
 * Handle API request
 */
class RequestHandler
{
    protected $startDate;
    protected $endDate;
    protected $tickers;
    protected $groupByWeek;

    /**
     * Execute API request processing
     */
    public function processApiRequest()
    {
        $this->startDate   =  (isset($_GET['startDate']) && $this->validateDate($_GET['startDate'])) ? $_GET['startDate'] : null;
        $this->endDate     =  (isset($_GET['endDate']) && $this->validateDate($_GET['endDate'])) ? $_GET['endDate'] : null;
        $this->tickers     =  (isset($_GET['tickers'])) ? $_GET['tickers'] : null;
        $this->groupByWeek = (isset($_GET['groupByWeek']) && $_GET['groupByWeek'] == 'true') ? $_GET['groupByWeek'] : null;

        if (!$this->validSubmittedParams()) {
            $result = json_encode([]);
        } else {
            $result = $this->processRequestData();
        }

        print $result;

    }

    /**
     * Prepare submitted data for DB query and send to DB processing
     */
    protected function processRequestData()
    { 
        $preppedSubmittedData = $this->prepSubmittedData();

        $dbResult = $this->getTickerDataFromDB($preppedSubmittedData);
        $result = json_encode($dbResult);

        return $result;
        
    }

    /**
     * Check that required params are present in GET variables
     */
    protected function validSubmittedParams()
    {
        $return = false;

        if (!empty($this->startDate) && !empty($this->endDate) && !empty($this->tickers)){
            $return = true;
        }

        return $return;
    }

    /**
     * Prepare submitted data for inclusion in the DB query
     */
    protected function prepSubmittedData()
    {
        $preppedData['startDate'] = addslashes($this->startDate);
        $preppedData['endDate'] = addslashes($this->endDate);

        if (is_array($this->tickers)) {
            foreach($this->tickers as $ticker)
            {
                $preppedData['tickers'][] = addslashes($ticker);
            }
        } else {
            $preppedData['tickers'][] = addslashes($this->tickers);
        }
        

        if(!is_null($this->groupByWeek)) {
            $preppedData['groupByWeek'] = addslashes($this->groupByWeek);
        }

        return $preppedData;
    }

    /**
     * Execute get data from DB process
     */
    protected function getTickerDataFromDB(array $preppedSubmittedData)
    {
        $databaseHandler = new DatabaseHandler();
        $foundTickerData = $databaseHandler->getTickerData($preppedSubmittedData);
        return $foundTickerData;
    }

    /**
     * Validate that submitted dates are in the right format
     */
    protected function validateDate($date)
    {
        $d = DateTime::createFromFormat('d/m/Y', $date);
        return $d && $d->format('d/m/Y') == $date;
    }
}